from graphql_geojson.filters import GeometryFilterSet
from .models import BusRoute, BusStop


class BusRouteFilter(GeometryFilterSet):

    class Meta:
        model = BusRoute
        fields = {
            'name': ('exact', 'icontains'),
            'route': ('intersects', 'distance_lte'),
        }


class BusStopFilter(GeometryFilterSet):

    class Meta:
        model = BusStop
        fields = {
            'point': ('distance_lte',),
        }
