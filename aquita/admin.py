from django.contrib.gis import admin
from .models import BusRoute, BusStop, BusTag


class BusTagAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


class BusRouteAdmin(admin.OSMGeoAdmin):
    exclude = ('created_at', 'updated_at', 'source')
    list_display = (
        'name', 'desc', 'notes', 'company', 'color', 'created_at',
        'updated_at',
    )
    date_hierarchy = 'updated_at'
    search_fields = ('name', 'desc', 'notes', 'company', 'color')
    list_filter = ('verified', 'company', 'color', 'tags')
    autocomplete_fields = ('tags',)


class BusStopAdmin(admin.OSMGeoAdmin):
    exclude = ('created_at', 'updated_at')
    list_display = (
        'point', 'name', 'osm_node_id', 'mapaton_id', 'source', 'created_at',
        'updated_at',
    )
    date_hierarchy = 'updated_at'
    list_filter = ('source', 'verified')
    search_fields = ('name', 'osm_node_id', 'mapaton_id')


admin.site.register(BusRoute, BusRouteAdmin)
admin.site.register(BusStop, BusStopAdmin)
admin.site.register(BusTag, BusTagAdmin)
