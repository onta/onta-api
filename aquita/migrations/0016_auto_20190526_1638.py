# Generated by Django 2.1.2 on 2019-05-26 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aquita', '0015_auto_20190526_0330'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='busroute',
            name='tags',
            field=models.ManyToManyField(related_name='bus_routes', to='aquita.BusTag'),
        ),
    ]
